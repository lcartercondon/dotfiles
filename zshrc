# Antigen
ANTIGEN_SOURCE=$(pacman -Ql $(pacman -Qsq antigen) | grep 'antigen.zsh' | awk '{print $2};')
if [ -e $ANTIGEN_SOURCE ]; then
	source $ANTIGEN_SOURCE

	antigen use oh-my-zsh

	antigen bundle adb
#	antigen bundle aws
	antigen bundle command-not-found
	antigen bundle docker
	antigen bundle docker-compose
	antigen bundle dotenv
	antigen bundle git
	antigen bundle knife
	antigen bundle kubectl

	# zsh-nvm settings
	export NVM_LAZY_LOAD=true
	export NVM_AUTO_USE=true
	antigen bundle lukechilds/zsh-nvm
	antigen bundle zsh-users/zsh-syntax-highlighting
	antigen apply
fi

# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=65535
SAVEHIST=65535
setopt appendhistory autocd extendedglob notify
unsetopt beep
bindkey -e
bindkey "${terminfo[khome]}" beginning-of-line
bindkey "${terminfo[kend]}" end-of-line
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '$HOME/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# Powerline
if [[ $(command -v powerline-daemon) ]]; then
	powerline-daemon -q
	. $(python -c "import site; print(site.getsitepackages()[0])")/powerline/bindings/zsh/powerline.zsh
fi

BASE16_SHELL=$HOME/.config/base16-shell/
# if not konsole, load the base16 profile using the profile helper
#if [[ "$TERM" != konsole* ]]; then # doesn't work if konsole profile sets TERM somehow
if [ -z ${KONSOLE_DBUS_WINDOW+x} ]; then
	[ -n "$PS1" ] && [ -s $BASE16_SHELL/profile_helper.sh ] && eval "$($BASE16_SHELL/profile_helper.sh)"
fi

export EDITOR=/usr/bin/nvim
source ~/.profile
source ~/.shell_aliases

mm() {
  if [ -z "$1"  ]; then
    echo "specify a host e.g. app-test or a000001"
  else
    echo "moshing to $1.5gdcs.com"
    mosh "$1.5gdcs.com"
  fi
}

pp() {
	if [ -z "$1" ]; then
		echo "specify a host e.g. app-test or a000001"
	else
		echo "pinging $1.5gdcs.com"
		ping "$1.5gdcs.com"
	fi
}

smtr() {
	if [ -z "$1" ]; then
		echo "specify a host e.g. app-test or a000001"
	else
		echo "starting traceroute to $1.5gdcs.com"
		mtr -4 --curses "$1.5gdcs.com"
	fi
}

docked() {
	# TODO: determine which docking configuration and set displays accordingly
	if [[ "$(hostname)" == "khanda" ]]; then
		xrandr --output eDP --mode 1920x1080 --rate 60.0 --pos 3840x0 --output DisplayPort-0 --mode 1920x1080 --rate 60.0 --pos 1920x0 --output DisplayPort-3 --primary --mode 1920x1080 --rate 60.0 --pos 0x0
	fi
}

undocked() {
	if [[ "$(hostname)" = "khanda" ]]; then
		xrandr --output eDP --primary --mode 1920x1080 --rate 60.0 --pos 0x0
	fi
}
## COMPLETION

###-begin-pm2-completion-###
### credits to npm for the completion file model
#
# Installation: pm2 completion >> ~/.bashrc  (or ~/.zshrc)
#

COMP_WORDBREAKS=${COMP_WORDBREAKS/=/}
COMP_WORDBREAKS=${COMP_WORDBREAKS/@/}
export COMP_WORDBREAKS

if type complete &>/dev/null; then
  _pm2_completion () {
    local si="$IFS"
    IFS=$'\n' COMPREPLY=($(COMP_CWORD="$COMP_CWORD" \
                           COMP_LINE="$COMP_LINE" \
                           COMP_POINT="$COMP_POINT" \
                           pm2 completion -- "${COMP_WORDS[@]}" \
                           2>/dev/null)) || return $?
    IFS="$si"
  }
  complete -o default -F _pm2_completion pm2
elif type compctl &>/dev/null; then
  _pm2_completion () {
    local cword line point words si
    read -Ac words
    read -cn cword
    let cword-=1
    read -l line
    read -ln point
    si="$IFS"
    IFS=$'\n' reply=($(COMP_CWORD="$cword" \
                       COMP_LINE="$line" \
                       COMP_POINT="$point" \
                       pm2 completion -- "${words[@]}" \
                       2>/dev/null)) || return $?
    IFS="$si"
  }
  compctl -K _pm2_completion + -f + pm2
fi
###-end-pm2-completion-###

# plugins=(knife)

# tabtab source for serverless package
# uninstall by removing these lines or running `tabtab uninstall serverless`
[[ -f $HOME/.cache/pacaur/nodejs-serverless/pkg/nodejs-serverless/usr/lib/node_modules/serverless/node_modules/tabtab/.completions/serverless.zsh ]] && . $HOME/.cache/pacaur/nodejs-serverless/pkg/nodejs-serverless/usr/lib/node_modules/serverless/node_modules/tabtab/.completions/serverless.zsh
# tabtab source for sls package
# uninstall by removing these lines or running `tabtab uninstall sls`
[[ -f $HOME/.cache/pacaur/nodejs-serverless/pkg/nodejs-serverless/usr/lib/node_modules/serverless/node_modules/tabtab/.completions/sls.zsh ]] && . $HOME/.cache/pacaur/nodejs-serverless/pkg/nodejs-serverless/usr/lib/node_modules/serverless/node_modules/tabtab/.completions/sls.zsh

# aws cli completion
# source $(command -v aws_zsh_completer.sh)

# tabtab source for slss package
# uninstall by removing these lines or running `tabtab uninstall slss`
[[ -f /home/liam/git/sestra/reancloud/rest/node_modules/tabtab/.completions/slss.zsh ]] && . /home/liam/git/sestra/reancloud/rest/node_modules/tabtab/.completions/slss.zsh