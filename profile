#if [ -e /usr/share/terminfo/x/xterm-256color ]; then
#	export TERM='xterm-256color'
#else
#	export TERM='xterm-color'
#fi
#export _JAVA_OPTIONS='-Dsun.java2d.opengl=true'
export _JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=on -Dswing.aatext=true -Dswing.defaultaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel'
